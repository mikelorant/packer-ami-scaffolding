# Packer-AMI-Scaffolding

## What is Packer-AMI-Scaffolding?

Packer repository being used as a proof of concept for an infrastructure build in AWS.

## Requirements

Packer-AMI-Scaffolding requires:

* Packer 0.5.2+

## Installation

Installing Packer-AMI-Scaffolding can be done using the following commands to clone the repository.

        git clone https://bitbucket.org/mikelorant/packer-ami-scaffolding.git
        cd packer-ami-scaffolding

The Packer binaries need to be installed in /usr/local/bin. The following location contains the latest release.

	http://www.packer.io/downloads.html

You need to create a variables.template file. The access key needs to provide full access to EC2 and S3. A bucket to store the AMI is also required.

	{
	  "aws_access_key": "KEY",
	  "aws_secret_key": "SECRET",
	  "aws_s3_bucket": "BUCKET"
	}

You will also need to copy the x509 certificate and key into the keys directory and update the template.json file. The X509 certificate must be registered with your account from the security credentials page in the AWS console.

## Usage

To start the build process.

	packer build -var-file=variables.json template.json

This will take approximately 10 minutes to complete.
